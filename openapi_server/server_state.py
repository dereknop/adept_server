"""
Tracks information we have received from users.
The main object of the application.
The endpoint handlers are defined in openapi_server/controllers/default_controller.py, but those
just mostly call into a corresponding function in this class.
"""
import logging
import math
import typing
import uuid
from collections import defaultdict

from openapi_server import alignment_score
from openapi_server import attribute_values
from openapi_server import data_loader
from openapi_server.models.alignment_results import AlignmentResults
from openapi_server.models.alignment_source import AlignmentSource
from openapi_server.models.kdma import KDMA
from openapi_server.models.kdma_name import KDMAName
from openapi_server.models.probe_response import ProbeResponse
from openapi_server.models.probe_response_batch import ProbeResponseBatch
from openapi_server.models.response import Response
from openapi_server.models.scenario import Scenario


class ServerState:
    """Represents the state of the server as clients interact with us.
    For example tracks sessions, holds responses, etc.
    """

    SessionIdType = str
    """A typedef for how we represent a session ID.  It is a uuid, but as a string."""

    def __init__(self) -> None:
        self._current_session_id: ServerState.SessionIdType | None = None
        """Current active session"""
        self._session_ids: set[ServerState.SessionIdType] = set()
        """All sessions so far"""
        self._probe_responses_by_session: dict[ServerState.SessionIdType, list[Response]] = dict()
        """{session id: responses given for that session}"""
        self._known_scenario_ids: typing.Final[list[str]] = data_loader.get_known_scenario_ids()
        """All scenario IDs the application supports."""
        self._known_probe_ids: typing.Final[dict[str, list[str]]] = data_loader.get_known_probe_ids()
        """All probe IDs the application supports."""

    def _change_session_id(self, session_id: SessionIdType) -> None:
        """ :param session_id: The session ID to change to. """
        self._session_ids.add(session_id)
        self._current_session_id = session_id
        return

    def _handle_session_id(self, session_id: SessionIdType | None) -> SessionIdType:
        """ Helper function for dealing if session ID was or wasn't given.
        If it was given, we'll assume we want to use that session ID.
        If it wasn't, we'll use the current ID, or generate a new one if there isn't a current.

        :param session_id: Proposed session ID.
        :return: Session ID to use.
        """
        if session_id is not None:
            self._change_session_id(session_id=session_id)
            return session_id
        if self._current_session_id is None:
            return self.generate_session_id()
        return self._current_session_id

    def generate_session_id(self) -> str:
        """ Generate a new session ID and set it as the active session.
        
        :return: The new session ID.
        """
        self._change_session_id(session_id=str(uuid.uuid4()))
        return self._current_session_id
    
    def add_probe_response(self, probe_response: ProbeResponse) -> None:
        """Take note of a new response to a single probe."""
        probe_response.session_id = self._handle_session_id(probe_response.session_id)
        assert probe_response.response.scenario_id in self._known_scenario_ids, "Unknown scenario"
        assert probe_response.response.probe_id in self._known_probe_ids[probe_response.response.scenario_id], "Unknown probe"
        if probe_response.session_id not in self._probe_responses_by_session:
            self._probe_responses_by_session[probe_response.session_id] = [probe_response.response]
        else:
            self._probe_responses_by_session[probe_response.session_id].append(probe_response.response)
        return
    
    def add_probe_responses(self, probe_responses: ProbeResponseBatch) -> None:
        """Take note of new responses to some number of probes."""
        probe_responses.session_id = self._handle_session_id(probe_responses.session_id)
        if probe_responses.session_id not in self._probe_responses_by_session:
            self._probe_responses_by_session[probe_responses.session_id] = []
        for response in probe_responses.responses:
            assert response.scenario_id in self._known_scenario_ids
            assert response.probe_id in self._known_probe_ids[response.scenario_id]
            self._probe_responses_by_session[probe_responses.session_id].append(response)
        return
    
    def get_alignment_result_probe(self, session_id: str, target_id: str, scenario_id: str, probe_id: str, population: int = 1) -> AlignmentResults:
        """Calculates an alignment for a single response previously given.

        :param session_id: The session that holds the response.
        :param target_id: The target we are aligning to.
        :param scenario_id: The scenario that is being processed.
        :param probe_id: The question that the response was to.
        :param population: If 0, will treat target as set of attribute values. If 1, will treat target as 
        attribute values for an entire population.
        :return: Info about the calculated attribute values and alignment score.
        """
        session_id = self._handle_session_id(session_id)
        responses: list[Response] | None = self._probe_responses_by_session.get(session_id, None)
        if responses is None:
            logging.error("Requested alignment but no responses have been sent for session %s.", session_id)
            return AlignmentResults()
        all_choices: list[str] = [response.choice for response in responses]
        kdma_values = None
        for response in responses:
            if response.scenario_id == scenario_id and response.probe_id == probe_id:
                if kdma_values is not None:
                    logging.warning("Multiple responses found for this session (%s), scenario (%s), and probe (%s) combo.  Using latest.", session_id, response.scenario_id, response.probe_id)
                kdma_values = attribute_values.compute_attribute_values(choice_id=response.choice, all_choices=all_choices)
        if kdma_values is None:
            logging.error("Requested alignment but no responses for probe %s (scenario %s) have been received in session %s.", probe_id, scenario_id, session_id)
            return AlignmentResults()
        
        if population:
            score = alignment_score.calculate_alignment_to_population(actual_attribute_values=kdma_values, alignment_target_distribution_id=target_id)
        else:
            score = alignment_score.calculate_alignment(actual_attribute_values=kdma_values, alignment_target_id=target_id)
        
        source = AlignmentSource(scenario_id=scenario_id, probes=[probe_id])
        result = AlignmentResults(alignment_source=[source],
                                  alignment_target_id=target_id,
                                  score=score,
                                  kdma_values=kdma_values)
        return result
    
    def get_alignment_result_session(self, session_id: str, target_id: str, population: int = 1) -> AlignmentResults:
        """Calculates an alignment for a collection of responses previously given.

        :param session_id: The session that holds the responses.
        :param target_id: The target we are aligning to.
        :param population: If 0, will treat target as set of attribute values. If 1, will treat target as 
        attribute values for an entire population.
        :return: Info about the calculated attribute values and alignment score.
        """
        session_id = self._handle_session_id(session_id)
        responses: list[Response] | None = self._probe_responses_by_session.get(session_id, None)
        if responses is None:
            logging.warning("Requested alignment but no responses have been sent for session %s", session_id)
            return AlignmentResults(alignment_source=[], alignment_target_id=target_id, score=0.5, kdma_values=[])
        all_choices: list[str] = [response.choice for response in responses]
        kdma_values = []
        scenario_and_probes = defaultdict(list)  # for each scenario used in the session, records all probes responded to
        for response in responses:
            kdma_values.append(attribute_values.compute_attribute_values(choice_id=response.choice, all_choices=all_choices))
            scenario_and_probes[response.scenario_id].append(response.probe_id)

        sources = [AlignmentSource(scenario_id=scenario_id, probes=list(probe_ids)) for scenario_id, probe_ids in scenario_and_probes.items()]
        kdma_values = attribute_values.merge_attribute_values(kdma_results=kdma_values)

        if population:
            score = alignment_score.calculate_alignment_to_population(actual_attribute_values=kdma_values, alignment_target_distribution_id=target_id)
        else:
            score = alignment_score.calculate_alignment(actual_attribute_values=kdma_values, alignment_target_id=target_id)

        result = AlignmentResults(alignment_source=sources,
                                  alignment_target_id=target_id,
                                  score=score,
                                  kdma_values=kdma_values)
        return result


SERVER_STATE = ServerState()
"""The object that represents the current state of the server."""


def main() -> None:
    """Example code exercising functionality."""
    server = ServerState()
    session_id = server.generate_session_id()
    scenario_id: typing.Final[str] = "MetricsEval.MD1-Urban"
    target_id: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-train-HIGH"

    r1 = Response(scenario_id=scenario_id,
                  probe_id="MetricsEval.MD1.1",
                  choice="MetricsEval.MD1.1.A")
    pr = ProbeResponse(session_id=session_id,
                       response=r1)
    server.add_probe_response(pr)

    r2 = Response(scenario_id=scenario_id,
                  probe_id="MetricsEval.MD1.2",
                  choice="MetricsEval.MD1.2.A")
    r3 = Response(scenario_id=scenario_id,
                  probe_id="MetricsEval.MD1.3",
                  choice="MetricsEval.MD1.3.A")
    prb = ProbeResponseBatch(session_id=session_id,
                             responses=[r2, r3])
    server.add_probe_responses(probe_responses=prb)

    ar1 = server.get_alignment_result_probe(session_id=session_id,
                                            target_id=target_id,
                                            scenario_id=scenario_id,
                                            probe_id="MetricsEval.MD1.1",
                                            population=0)
    ar2 = server.get_alignment_result_probe(session_id=session_id,
                                            target_id=target_id,
                                            scenario_id=scenario_id,
                                            probe_id="MetricsEval.MD1.2",
                                            population=0)
    ar3 = server.get_alignment_result_probe(session_id=session_id,
                                            target_id=target_id,
                                            scenario_id=scenario_id,
                                            probe_id="MetricsEval.MD1.3",
                                            population=0)
    
    ar4 = server.get_alignment_result_session(session_id=session_id,
                                              target_id=target_id,
                                              population=0)

    # Again
    new_session = server.generate_session_id()
    r1 = Response(scenario_id=scenario_id,
                  probe_id="MetricsEval.MD1.1",
                  choice="MetricsEval.MD1.1.A")
    pr = ProbeResponse(session_id=new_session,
                       response=r1)
    server.add_probe_response(pr)    
    ar1 = server.get_alignment_result_probe(session_id=new_session,
                                            target_id=target_id,
                                            scenario_id=scenario_id,
                                            probe_id="MetricsEval.MD1.1",
                                            population=0)
    assert math.isclose(ar1.score, 0.5)
    ar2 = server.get_alignment_result_session(session_id=new_session,
                                              target_id=target_id,
                                              population=0)
    assert math.isclose(ar2.score, 0.5)

    return


if __name__ == '__main__':
    main()
