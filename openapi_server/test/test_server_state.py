"""Test manipulating the state of the server"""
import typing
import unittest

from openapi_server.models.probe_response import ProbeResponse
from openapi_server.models.probe_response_batch import ProbeResponseBatch
from openapi_server.models.response import Response
from openapi_server.server_state import ServerState


class TestServerState(unittest.TestCase):
    """testing server_state.py"""

    def test_stuff(self) -> None:
        server = ServerState()
        session_id = server.generate_session_id()
        scenario_id: typing.Final[str] = "MetricsEval.MD18"
        target_id: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-train-HIGH"

        r1 = Response(scenario_id=scenario_id,
                      probe_id="MetricsEval.MD18.1",
                      choice="MetricsEval.MD18.1.A")
        pr = ProbeResponse(session_id=session_id,
                           response=r1)
        server.add_probe_response(pr)

        r2 = Response(scenario_id=scenario_id,
                      probe_id="MetricsEval.MD18.2",
                      choice="MetricsEval.MD18.2.B")
        r3 = Response(scenario_id=scenario_id,
                      probe_id="MetricsEval.MD18.3",
                      choice="MetricsEval.MD18.3.A")
        prb = ProbeResponseBatch(session_id=session_id,
                                 responses=[r2, r3])
        server.add_probe_responses(probe_responses=prb)

        ar1 = server.get_alignment_result_probe(session_id=session_id,
                                                target_id=target_id,
                                                scenario_id=scenario_id,
                                                probe_id="MetricsEval.MD18.1",
                                                population=0)
        self.assertAlmostEqual(ar1.score, 0.5)  # value .5 vs target 1
        ar1b = server.get_alignment_result_probe(session_id=session_id,
                                                 target_id=target_id,
                                                 scenario_id=scenario_id,
                                                 probe_id="MetricsEval.MD18.1",
                                                 population=1)
        self.assertLess(ar1b.score, 0.01)  # value .5 vs target 1 distribution
        self.assertGreaterEqual(ar1b.score, 0.0)  # value .5 vs target 1 distribution
        ar2 = server.get_alignment_result_probe(session_id=session_id,
                                                target_id=target_id,
                                                scenario_id=scenario_id,
                                                probe_id="MetricsEval.MD18.2",
                                                population=0)
        self.assertAlmostEqual(ar2.score, 1.0)  # value 1 vs target 1
        ar2b = server.get_alignment_result_probe(session_id=session_id,
                                                 target_id=target_id,
                                                 scenario_id=scenario_id,
                                                 probe_id="MetricsEval.MD18.2",
                                                 population=1)
        self.assertGreater(ar2b.score, 0.99)  # value 1 vs target 1 distribution
        self.assertLessEqual(ar2b.score, 1.0)
        ar3 = server.get_alignment_result_probe(session_id=session_id,
                                                target_id=target_id,
                                                scenario_id=scenario_id,
                                                probe_id="MetricsEval.MD18.3",
                                                population=0)
        self.assertAlmostEqual(ar3.score, 0)  # score of 0 against alignment of 1
        ar3b = server.get_alignment_result_probe(session_id=session_id,
                                                 target_id=target_id,
                                                 scenario_id=scenario_id,
                                                 probe_id="MetricsEval.MD18.3",
                                                 population=1)
        self.assertAlmostEqual(ar3b.score, 0)  # score of 0 against alignment of 1

        ar4 = server.get_alignment_result_session(session_id=session_id, target_id=target_id, population=0)
        self.assertAlmostEqual(ar4.score, 0.5)  # 0.5, 1, 0

        # Again
        new_session = server.generate_session_id()
        r1 = Response(scenario_id=scenario_id,
                      probe_id="MetricsEval.MD18.1",
                      choice="MetricsEval.MD18.1.A")
        pr = ProbeResponse(session_id=new_session,
                           response=r1)
        server.add_probe_response(pr)
        ar1 = server.get_alignment_result_probe(session_id=new_session,
                                                target_id=target_id,
                                                scenario_id=scenario_id,
                                                probe_id="MetricsEval.MD18.1",
                                                population=0)
        self.assertAlmostEqual(ar1.score, 0.5)
        ar2 = server.get_alignment_result_session(session_id=new_session,
                                                  target_id=target_id,
                                                  population=0)
        self.assertAlmostEqual(ar2.score, 0.5)
        return


if __name__ == "__main__":
    unittest.main()
