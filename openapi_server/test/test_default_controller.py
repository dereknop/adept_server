import logging
import typing
import unittest

from flask import json

import openapi_server.data_loader
from openapi_server.models.alignment_results import AlignmentResults
from openapi_server.models.alignment_source import AlignmentSource
from openapi_server.models.alignment_target import AlignmentTarget  # noqa: E501
from openapi_server.models.alignment_target_distribution import AlignmentTargetDistribution  # noqa: E501
from openapi_server.models.get_alignment_target_api_v1_alignment_target_target_id_get200_response import GetAlignmentTargetApiV1AlignmentTargetTargetIdGet200Response  # noqa: E501
from openapi_server.models.http_validation_error import HTTPValidationError
from openapi_server.models.kdma import KDMA
from openapi_server.models.kdma_name import KDMAName  # noqa: E501
from openapi_server.models.probe_response import ProbeResponse  # noqa: E501
from openapi_server.models.probe_response_batch import ProbeResponseBatch
from openapi_server.models.response import Response  # noqa: E501
from openapi_server.models.scenario import Scenario  # noqa: E501
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    _target_id: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-train-HIGH"
    _distribution_id: typing.Final[str] = "ADEPT-metrics_eval-alignment-target-pop-eval-HIGH"
    _scenario_id: typing.Final[str] = "MetricsEval.MD3"
    _probe_id: typing.Final[str] = "MetricsEval.MD3.1"
    _choice_id: typing.Final[str] = "MetricsEval.MD3.1.A"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._responses_sent = 0

    def test_get_alignment_target_api_v1_alignment_target_target_id_get(self):
        """Test case for get_alignment_target_api_v1_alignment_target_target_id_get

        Get Alignment Target
        """
        query_string = [('population', False)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment_target/{target_id}'.format(target_id=self.__class__._target_id),
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get(self):
        """Test case for get_alignment_target_distribution_api_v1_alignment_target_distribution_target_id_get

        Get Alignment Target Distribution
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment_target_distribution/{target_id}'.format(target_id=self.__class__._distribution_id),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get(self):
        """Test case for get_alignment_target_distribution_ids_api_v1_alignment_target_distribution_ids_get

        Get alignment targets distribution IDs
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment_target_distribution_ids',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_alignment_target_ids_api_v1_alignment_target_ids_get(self):
        """Test case for get_alignment_target_ids_api_v1_alignment_target_ids_get

        Get alignment targets IDs
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment_target_ids',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_api_api_v1_get(self):
        """Test case for get_api_api_v1_get

        Get Api
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
  
    def test_post_new_session_id_api_v1_new_session_post(self):
        """Test case for post_new_session_id_api_v1_new_session_post

        Post New Session Id
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/new_session',
            method='POST',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_post_probe_response_api_v1_response_post(self):
        """Test case for post_probe_response_api_v1_response_post

        Post Probe Response
        """
        probe_response = {"response":{"probe_id":self.__class__._probe_id,"justification":"justification","choice":self.__class__._choice_id,"scenario_id":self.__class__._scenario_id},"session_id":""}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/api/v1/response',
            method='POST',
            headers=headers,
            data=json.dumps(probe_response),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self._responses_sent += 1

    def test_post_probe_responses_api_v1_responses_post(self):
        """Test case for post_probe_responses_api_v1_responses_post

        Post Probe Responses
        """
        probe_response_batch = {"session_id":"","responses":[{"probe_id":"MetricsEval.MD-Jungle.1","justification":"justification","choice":"MetricsEval.MD-Jungle.1.A","scenario_id":"MetricsEval.MD4-Jungle"},{"probe_id":"MetricsEval.MD-Jungle.2","justification":"justification","choice":"MetricsEval.MD-Jungle.2.B","scenario_id":"MetricsEval.MD4-Jungle"}]}
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/api/v1/responses',
            method='POST',
            headers=headers,
            data=json.dumps(probe_response_batch),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self._responses_sent += 1

    def test_get_probe_response_alignment_api_v1_alignment_probe_get(self):
        """Test case for get_probe_response_alignment_api_v1_alignment_probe_get

        Get Probe Response Alignment
        """
        if self._responses_sent < 1:
            self.test_post_probe_response_api_v1_response_post()
        if self._responses_sent < 2:
            self.test_post_probe_responses_api_v1_responses_post()

        query_string = [('session_id', ''),
                        ('target_id', self.__class__._target_id),
                        ('scenario_id', self.__class__._scenario_id),
                        ('probe_id', self.__class__._probe_id),
                        ('population', False)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment/probe',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_scenario_api_v1_scenario_scenario_id_get(self):
        """Test case for get_scenario_api_v1_scenario_scenario_id_get

        Get Scenario
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/scenario/{scenario_id}'.format(scenario_id=self.__class__._scenario_id),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_session_alignment_api_v1_alignment_session_get(self):
        """Test case for get_session_alignment_api_v1_alignment_session_get

        Get Session Alignment
        """
        if self._responses_sent < 1:
            self.test_post_probe_response_api_v1_response_post()
        if self._responses_sent < 2:
            self.test_post_probe_responses_api_v1_responses_post()
        query_string = [('session_id', ""),
                        ('target_id', self.__class__._target_id),
                        ('population', False)]
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/api/v1/alignment/session',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    # Custom test cases

    def _get_scenario(self, scenario_id: str) -> None:
        """ GET a scenario """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.get(
            f'/api/v1/scenario/{scenario_id}',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(Scenario.from_dict(json.loads(response.data.decode('utf-8'))),
                         openapi_server.data_loader.load_object(Scenario, scenario_id))
        return None

    def test_typical_metrics_eval_use_case(self) -> None:
        """Tests various API on a run of a metric eval training scenario"""
        current_scenario = "MetricsEval.MD3"
        self._get_scenario(current_scenario)
        
        # Get alignment target
        headers = { 
            'Accept': 'application/json',
        }
        target_id: typing.Final[str] = 'ADEPT-metrics_eval-alignment-target-train-HIGH'
        response = self.client.get(
            f'/api/v1/alignment_target/{target_id}',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(AlignmentTarget.from_dict(json.loads(response.data.decode('utf-8'))),
                         openapi_server.data_loader.load_object(AlignmentTarget, target_id))
        
        # Start a session
        response = self.client.post(
            '/api/v1/new_session',
            headers=headers)
        self.assert200(response)
        session_id = json.loads(response.data.decode('utf-8'))
        self.assertTrue(isinstance(session_id, str))

        # Make response
        more_headers = { 'Accept': 'application/json', 'Content-Type': 'application/json',}
        response_msg = {
            "response": {
                "choice": "MetricsEval.MD3.1.C",
                "justification": "justification",
                "probe_id": "MetricsEval.MD3.1",
                "scenario_id": current_scenario
            },
            "session_id": session_id
        }
        response = self.client.post(
            '/api/v1/response',
            headers=more_headers,
            data=json.dumps(response_msg))
        self.assert200(response)

        # Get alignment
        response = self.client.get(
            '/api/v1/alignment/session',
            query_string={
                'session_id': session_id,
                'target_id': target_id
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        data = json.loads(response.data.decode('utf-8'))
        alignment_result = AlignmentResults.from_dict(data)
        self.assertEqual(len(alignment_result.kdma_values), 1)
        self.assertEqual(alignment_result.kdma_values[0].kdma, KDMAName.MORALDESERT)
        self.assertAlmostEqual(alignment_result.kdma_values[0].value, 1.0)

        self._get_scenario("MetricsEval.MD17")
        self._get_scenario("MetricsEval.MD18")

        choices = ["MetricsEval.MD17.1.A",
                   "MetricsEval.MD17.2.A",
                   "MetricsEval.MD17.3.A",
                   "MetricsEval.MD17.4.A",
                   "MetricsEval.MD17.5.B",
                   "MetricsEval.MD17.6.B",
                   "MetricsEval.MD17.7.B",
                   "MetricsEval.MD17.8.B",
                   "MetricsEval.MD17.9.B",
                   "MetricsEval.MD17.10.B",
                   "MetricsEval.MD17.11.B",
                   "MetricsEval.MD17.12.B",
                   "MetricsEval.MD17.13.B",
                   "MetricsEval.MD17.14.B",
                   "MetricsEval.MD17.15.B",
                   "MetricsEval.MD17.16.B",
                   "MetricsEval.MD17.17.B",
                   "MetricsEval.MD17.18.B",
                   "MetricsEval.MD17.19.B",
                   "MetricsEval.MD17.20.B",
                   "MetricsEval.MD17.21.A",
                   "MetricsEval.MD17.22.C",
                   "MetricsEval.MD18.1.A",
                   "MetricsEval.MD18.2.B",
                   "MetricsEval.MD18.3.B",
                   "MetricsEval.MD18.4.B",
                   "MetricsEval.MD18.5.B",
                   "MetricsEval.MD18.6.B",
                   "MetricsEval.MD18.7.B",
                   "MetricsEval.MD18.8.B",
                   "MetricsEval.MD18.9.B"]
        responses = [Response(scenario_id=choice.rsplit(".", 2)[0], probe_id=choice.rsplit(".", 1)[0], choice=choice) for choice in choices]
        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        response = self.client.post(
            '/api/v1/responses',
            headers=more_headers,
            data=json.dumps(prb))
        self.assert200(response)

        # Get final alignment
        response = self.client.get(
            '/api/v1/alignment/session',
            query_string={
                'session_id': session_id,
                'target_id': target_id
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        calculated_data = json.loads(response.data.decode('utf-8'))
        calculated_alignment_result = AlignmentResults.from_dict(calculated_data)
        self.assertEqual(len(calculated_alignment_result.alignment_source), 3)
        # Here we'll assume a certain order which may not actually be guaranteed by the API, but in reality this is what we are seeing
        self.assertEqual(calculated_alignment_result.alignment_source[0], AlignmentSource(scenario_id='MetricsEval.MD3', probes=['MetricsEval.MD3.1']))
        self.assertEqual(calculated_alignment_result.alignment_source[1].scenario_id, 'MetricsEval.MD17')
        self.assertListEqual(calculated_alignment_result.alignment_source[1].probes, [f"MetricsEval.MD17.{probe_num}" for probe_num in range(1, 23)])
        self.assertEqual(calculated_alignment_result.alignment_source[2].scenario_id, 'MetricsEval.MD18')
        self.assertListEqual(calculated_alignment_result.alignment_source[2].probes, [f"MetricsEval.MD18.{probe_num}" for probe_num in range(1, 10)])
        self.assertEqual(calculated_alignment_result.alignment_target_id, target_id)
        self.assertListEqual(calculated_alignment_result.kdma_values, [KDMA(kdma=KDMAName.MORALDESERT, value=1.0)])
        self.assertEqual(calculated_alignment_result.score, 1.0)

        # Start a session for eval
        response = self.client.post(
            '/api/v1/new_session',
            headers=headers)
        self.assert200(response)
        session_id = json.loads(response.data.decode('utf-8'))
        self.assertTrue(isinstance(session_id, str))

        # Get alignment target
        headers = { 
            'Accept': 'application/json',
        }
        target_id: typing.Final[str] = 'ADEPT-metrics_eval-alignment-target-eval-HIGH'
        response = self.client.get(
            f'/api/v1/alignment_target/{target_id}',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        self.assertEqual(AlignmentTarget.from_dict(json.loads(response.data.decode('utf-8'))),
                         openapi_server.data_loader.load_object(AlignmentTarget, target_id))
        
        eval_scenarios = ("MetricsEval.MD1-Urban", "MetricsEval.MD5-Desert", "MetricsEval.MD6-Submarine", "MetricsEval.MD4-Jungle")
        for s in eval_scenarios:
            self._get_scenario(s)
        choices = ["MetricsEval.MD1.1.A",
                   "MetricsEval.MD1.2.B",
                   "MetricsEval.MD1.3.B",
                   "MetricsEval.MD5.1.B",
                   "MetricsEval.MD5.2.A",
                   "MetricsEval.MD5.3.B",
                   "MetricsEval.MD6.1.A",
                   "MetricsEval.MD6.2.B",
                   "MetricsEval.MD6.3.B",
                   "MetricsEval.MD-Jungle.1.A",
                   "MetricsEval.MD-Jungle.2.B",
                   "MetricsEval.MD-Jungle.3.B"]
        def _add_environment_type(scenario_name_base: str) -> str:
            match scenario_name_base:
                case "MetricsEval.MD1":
                    return scenario_name_base + "-Urban"
                case "MetricsEval.MD5":
                    return scenario_name_base + "-Desert"
                case "MetricsEval.MD6":
                    return scenario_name_base + "-Submarine"
                case "MetricsEval.MD-Jungle":
                    return "MetricsEval.MD4-Jungle"
                case "MetricsEval.MD4-Jungle":
                    return "MetricsEval.MD4-Jungle"
                case _:
                    logging.error("Unexpected base %s", scenario_name_base)
                    return scenario_name_base
        responses = [Response(scenario_id=_add_environment_type(choice.rsplit(".", 2)[0]), probe_id=choice.rsplit(".", 1)[0], choice=choice) for choice in choices]

        prb = ProbeResponseBatch(session_id=session_id, responses=responses)
        response = self.client.post(
            '/api/v1/responses',
            headers=more_headers,
            data=json.dumps(prb))
        self.assert200(response)

        # Get final alignment
        response = self.client.get(
            '/api/v1/alignment/session',
            query_string={
                'session_id': session_id,
                'target_id': target_id
            },
            headers=headers
        )
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))
        calculated_data = json.loads(response.data.decode('utf-8'))
        calculated_alignment_result = AlignmentResults.from_dict(calculated_data)
        self.assertEqual(len(calculated_alignment_result.alignment_source), len(eval_scenarios))
        # Here we'll assume a certain order which may not actually be guaranteed by the API, but in reality this is what we are seeing
        self.assertEqual(calculated_alignment_result.alignment_source[0].scenario_id, _add_environment_type('MetricsEval.MD1'))
        self.assertListEqual(calculated_alignment_result.alignment_source[0].probes, [f"MetricsEval.MD1.{probe_num}" for probe_num in range(1, 4)])
        self.assertEqual(calculated_alignment_result.alignment_source[1].scenario_id, _add_environment_type('MetricsEval.MD5'))
        self.assertListEqual(calculated_alignment_result.alignment_source[1].probes, [f"MetricsEval.MD5.{probe_num}" for probe_num in range(1, 4)])
        self.assertEqual(calculated_alignment_result.alignment_source[2].scenario_id, _add_environment_type('MetricsEval.MD6'))
        self.assertListEqual(calculated_alignment_result.alignment_source[2].probes, [f"MetricsEval.MD6.{probe_num}" for probe_num in range(1, 4)])
        self.assertEqual(calculated_alignment_result.alignment_source[3].scenario_id, _add_environment_type('MetricsEval.MD4-Jungle'))
        self.assertListEqual(calculated_alignment_result.alignment_source[3].probes, [f"MetricsEval.MD-Jungle.{probe_num}" for probe_num in range(1, 4)])
        self.assertEqual(calculated_alignment_result.alignment_target_id, target_id)
        self.assertEqual(len(calculated_alignment_result.kdma_values), 1)
        self.assertEqual(calculated_alignment_result.kdma_values[0].kdma, KDMAName.MORALDESERT)
        self.assertAlmostEqual(calculated_alignment_result.kdma_values[0].value, 0.84444, delta=0.005)
        self.assertAlmostEqual(calculated_alignment_result.score, 1.0, delta=0.005)
        return


if __name__ == '__main__':
    unittest.main()
