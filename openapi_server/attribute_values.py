"""Calculates attribute values."""

import logging
from collections import defaultdict
from statistics import mean, fmean

from openapi_server.models.kdma import KDMA
from openapi_server.models.kdma_name import KDMAName


def _determine_value_for_choice(choice: str, all_choices: list[str]) -> float:
    match choice:
        # MD1 ubran shooter/victim
        case "MetricsEval.MD1.1.A":
            return 0.5
        case "MetricsEval.MD1.1.B":
            return 0.5
        
        case "MetricsEval.MD1.2.A":
            return 0.2
        case "MetricsEval.MD1.2.B":
            return 0.7
        
        case "MetricsEval.MD1.3.A":
            if "MetricsEval.MD1.2.A" in all_choices:
                return 0.1
            elif "MetricsEval.MD1.2.B" in all_choices:
                return 0.3
            else:
                logging.warning("%s without .2", choice)
                return fmean((0.1, 0.3))
        case "MetricsEval.MD1.3.B":
            if "MetricsEval.MD1.2.A" in all_choices:
                return 0.8
            elif "MetricsEval.MD1.2.B" in all_choices:
                return 0.9
            else:
                logging.warning("%s without .2", choice)
                return fmean((0.8, 0.9))
        
        # MD5 desert fistfight
        case "MetricsEval.MD5.1.A":
            return 0.3
        case "MetricsEval.MD5.1.B":
            return 0.8
        
        case "MetricsEval.MD5.2.A":
            if "MetricsEval.MD5.1.A" in all_choices:
                return 0.1
            elif "MetricsEval.MD5.1.B" in all_choices:
                return 0.5
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.1, 0.5))
        case "MetricsEval.MD5.2.B":
            if "MetricsEval.MD5.1.A" in all_choices:
                return 0.6
            elif "MetricsEval.MD5.1.B" in all_choices:
                return 0.5
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.6, 0.5))
        
        case "MetricsEval.MD5.3.A":
            if "MetricsEval.MD5.1.A" in all_choices:
                if "MetricsEval.MD5.2.A" in all_choices:
                    return 0.0
                elif "MetricsEval.MD5.2.B" in all_choices:
                    return 0.5
                else:
                    logging.warning("%s and 1.A without .2", choice)
                    return fmean((0.0, 0.5))
            elif "MetricsEval.MD5.1.B" in all_choices:
                if "MetricsEval.MD5.2.A" in all_choices:
                    return 0.4
                elif "MetricsEval.MD5.2.B" in all_choices:
                    return 0.5
                else:
                    logging.warning("%s and 1.B without .2", choice)
                    return fmean((0.4, 0.5))
            elif "MetricsEval.MD5.2.A" in all_choices:
                logging.warning("%s and 2.A without .1", choice)
                return fmean((0.0, 0.4))
            elif "MetricsEval.MD5.2.B" in all_choices:
                logging.warning("%s and 2.B without .1", choice)
                return fmean((0.5, 0.5))
            else:
                logging.warning("%s without .1 and .2", choice)
                return fmean((0.0, 0.5, 0.4, 0.5))
        case "MetricsEval.MD5.3.B":
            if "MetricsEval.MD5.1.A" in all_choices:
                if "MetricsEval.MD5.2.A" in all_choices:
                    return 0.6
                elif "MetricsEval.MD5.2.B" in all_choices:
                    return 0.7
                else:
                    logging.warning("%s and 1.A without .2", choice)
                    return fmean((0.6, 0.7))
            elif "MetricsEval.MD5.1.B" in all_choices:
                if "MetricsEval.MD5.2.A" in all_choices:
                    return 0.9
                elif "MetricsEval.MD5.2.B" in all_choices:
                    return 0.1
                else:
                    logging.warning("%s and 1.B without .2", choice)
                    return fmean((0.9, 0.1))
            elif "MetricsEval.MD5.2.A" in all_choices:
                logging.warning("%s and 2.A without .1", choice)
                return fmean((0.6, 0.9))
            elif "MetricsEval.MD5.2.B" in all_choices:
                logging.warning("%s and 2.B without .1", choice)
                return fmean((0.7, 0.1))
            else:
                logging.warning("%s without .1 and .2", choice)
                return fmean((0.6, 0.7, 0.9, 0.1))
        
        # MD6 submarine electrical accident
        case "MetricsEval.MD6.1.A":
            return 0.9
        case "MetricsEval.MD6.1.B":
            return 0.4
        
        case "MetricsEval.MD6.2.A":
            return 0.4
        case "MetricsEval.MD6.2.B":
            return 0.9
        
        case "MetricsEval.MD6.3.A":
            return 0.3
        case "MetricsEval.MD6.3.B":
            return 0.9
        
        # MD jungle IED
        case "MetricsEval.MD-Jungle.1.A":
            return 0.5
        case "MetricsEval.MD-Jungle.1.B":
            return 0.5
        
        case "MetricsEval.MD-Jungle.2.A":
            if "MetricsEval.MD-Jungle.1.A" in all_choices:
                return 0.1
            elif "MetricsEval.MD-Jungle.1.B" in all_choices:
                return 0.5
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.1, 0.5))
        case "MetricsEval.MD-Jungle.2.B":
            if "MetricsEval.MD-Jungle.1.A" in all_choices:
                return 0.9
            elif "MetricsEval.MD-Jungle.1.B" in all_choices:
                return 0.5
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.9, 0.5))
        
        case "MetricsEval.MD-Jungle.3.A":
            if "MetricsEval.MD-Jungle.1.A" in all_choices:
                return 0.3
            elif "MetricsEval.MD-Jungle.1.B" in all_choices:
                return 0.3
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.3, 0.3))
        case "MetricsEval.MD-Jungle.3.B":
            if "MetricsEval.MD-Jungle.1.A" in all_choices:
                return 0.7
            elif "MetricsEval.MD-Jungle.1.B" in all_choices:
                return 0.8
            else:
                logging.warning("%s without .1", choice)
                return fmean((0.7, 0.8))
            
        # MD Overrun position
        case "MetricsEval.MD-OverrunPosition.1.A":
            return 0.4
        case "MetricsEval.MD-OverrunPosition.1.B":
            return 0.9
        
        case "MetricsEval.MD-OverrunPosition.2.A":
            return 0.3
        case "MetricsEval.MD-OverrunPosition.2.B":
            return 0.8
        
        case "MetricsEval.MD-OverrunPosition.3.A":
            return 0.4
        case "MetricsEval.MD-OverrunPosition.3.B":
            return 0.7
       
        # MD3 Military base fire
        case "MetricsEval.MD3.1.A":
            return 0.0
        case "MetricsEval.MD3.1.B":
            return 0.0
        case "MetricsEval.MD3.1.C":
            return 1.0
        
        # MD17 Multi-Patient tradeoffs moral/lives
        case "MetricsEval.MD17.1.A" | "MetricsEval.MD17.2.A" | "MetricsEval.MD17.3.A" | "MetricsEval.MD17.4.A":
            return 0.5
        case "MetricsEval.MD17.1.B" | "MetricsEval.MD17.2.B" | "MetricsEval.MD17.3.B" | "MetricsEval.MD17.4.B":
            return 0.5

        case "MetricsEval.MD17.5.A" | "MetricsEval.MD17.6.A" | "MetricsEval.MD17.7.A" | "MetricsEval.MD17.8.A" | "MetricsEval.MD17.9.A" | "MetricsEval.MD17.10.A" | "MetricsEval.MD17.11.A" | "MetricsEval.MD17.12.A" | "MetricsEval.MD17.13.A" | "MetricsEval.MD17.14.A" | "MetricsEval.MD17.15.A" | "MetricsEval.MD17.16.A" | "MetricsEval.MD17.17.A" | "MetricsEval.MD17.18.A" | "MetricsEval.MD17.19.A" | "MetricsEval.MD17.20.A":
            return 0.0
        case "MetricsEval.MD17.5.B" | "MetricsEval.MD17.6.B" | "MetricsEval.MD17.7.B" | "MetricsEval.MD17.8.B" | "MetricsEval.MD17.9.B" | "MetricsEval.MD17.10.B" | "MetricsEval.MD17.11.B" | "MetricsEval.MD17.12.B" | "MetricsEval.MD17.13.B" | "MetricsEval.MD17.14.B" | "MetricsEval.MD17.15.B" | "MetricsEval.MD17.16.B" | "MetricsEval.MD17.17.B" | "MetricsEval.MD17.18.B" | "MetricsEval.MD17.19.B" | "MetricsEval.MD17.20.B":
            return 1.0

        case "MetricsEval.MD17.21.A":
            return 0.5
        case "MetricsEval.MD17.21.B":
            return 0.5
        case "MetricsEval.MD17.21.C":
            return 0.5

        case "MetricsEval.MD17.22.A":
            return 0.0
        case "MetricsEval.MD17.22.B":
            return 0.0
        case "MetricsEval.MD17.22.C":
            return 1.0

        # MD18 Multi-Patient tradeoffs moral/ingroup
        case "MetricsEval.MD18.1.A":
            return 0.5
        case "MetricsEval.MD18.1.B":
            return 0.5

        case "MetricsEval.MD18.2.A" | "MetricsEval.MD18.3.A" | "MetricsEval.MD18.4.A" | "MetricsEval.MD18.5.A" | "MetricsEval.MD18.6.A" | "MetricsEval.MD18.7.A" | "MetricsEval.MD18.8.A" | "MetricsEval.MD18.9.A":
            return 0.0
        case "MetricsEval.MD18.2.B" | "MetricsEval.MD18.3.B" | "MetricsEval.MD18.4.B" | "MetricsEval.MD18.5.B" | "MetricsEval.MD18.6.B" | "MetricsEval.MD18.7.B" | "MetricsEval.MD18.8.B" | "MetricsEval.MD18.9.B":
            return 1.0
        
        # MD20 Multi-Patient Tradeoffs evac
        case "MetricsEval.MD20.1.A":
            return 0.5
        case "MetricsEval.MD20.1.B":
            return 0.5
        case "MetricsEval.MD20.2.A":
            return 0.5
        case "MetricsEval.MD20.2.B":
            return 0.5
        case "MetricsEval.MD20.3.A":
            return 0.5
        case "MetricsEval.MD20.3.B":
            return 0.5
        case "MetricsEval.MD20.4.A":
            return 0.5
        case "MetricsEval.MD20.4.B":
            return 0.5
        case "MetricsEval.MD20.5.A":
            return 0.0
        case "MetricsEval.MD20.5.B":
            return 1.0
        case "MetricsEval.MD20.6.A":
            return 0.0
        case "MetricsEval.MD20.6.B":
            return 1.0
        case "MetricsEval.MD20.7.A":
            return 0.0
        case "MetricsEval.MD20.7.B":
            return 1.0
        case "MetricsEval.MD20.8.A":
            return 0.0
        case "MetricsEval.MD20.8.B":
            return 1.0
        case "MetricsEval.MD20.9.A":
            return 0.0
        case "MetricsEval.MD20.9.B":
            return 1.0
        case "MetricsEval.MD20.10.A":
            return 0.0
        case "MetricsEval.MD20.10.B":
            return 1.0
        case "MetricsEval.MD20.11.A":
            return 0.0
        case "MetricsEval.MD20.11.B":
            return 1.0
        case "MetricsEval.MD20.12.A":
            return 0.0
        case "MetricsEval.MD20.12.B":
            return 1.0
        case "MetricsEval.MD20.13.A":
            return 0.0
        case "MetricsEval.MD20.13.B":
            return 1.0
        case "MetricsEval.MD20.14.A":
            return 0.0
        case "MetricsEval.MD20.14.B":
            return 1.0
        case "MetricsEval.MD20.15.A":
            return 0.0
        case "MetricsEval.MD20.15.B":
            return 1.0
        case "MetricsEval.MD20.16.A":
            return 0.0
        case "MetricsEval.MD20.16.B":
            return 1.0
        case "MetricsEval.MD20.17.A":
            return 0.0
        case "MetricsEval.MD20.17.B":
            return 1.0
        case "MetricsEval.MD20.18.A":
            return 0.0
        case "MetricsEval.MD20.18.B":
            return 1.0
        case "MetricsEval.MD20.19.A":
            return 0.0
        case "MetricsEval.MD20.19.B":
            return 1.0
        case "MetricsEval.MD20.20.A":
            return 0.0
        case "MetricsEval.MD20.20.B":
            return 1.0

        # Unexpected option
        case _:
            logging.error("Unrecognized choice %s", choice)
            return 0.5


def _legal_kdma_names() -> list[str]:
    """Extracts the allowed enum values for KDMA names"""
    class_values = [v for k, v in vars(KDMAName).items() if not k.startswith('__')]
    return class_values


def get_kdma(name: str, kdmas: list[KDMA]) -> KDMA | None:
    """
    :param name: The name of the attribute to fetch.
    :param kdmas: The list of attributes and their values.
    :return: The requested attribute and its value.  None if not in list.
    """
    for kdma in kdmas:
        if kdma.kdma == name:
            return kdma
    return None


def compute_attribute_values(choice_id: str, all_choices: list[str]) -> list[KDMA]:
    """Given a choice, calculate the attribute values for that choice.

    :param choice_id: The ID of the ProbeChoice (option) chosen.
    :param all_choices: A list of all the IDs of ProbeChoice (options) chosen in the same session.
    :return: A list of KDMAs.  Each KDMA name will only be present at most once.
      A KDMA name not being in the list indicates the choice did not affect that attribute's value.
    """
    attribute_values: dict[str, float] = {KDMAName.MORALDESERT: _determine_value_for_choice(choice=choice_id, all_choices=all_choices)}

    kdmas = [KDMA(kdma=name, value=value) for name, value in attribute_values.items()]

    # Sanity check
    valid_names = _legal_kdma_names()
    assert all(kdma.kdma in valid_names for kdma in kdmas)  # It seems like the KDMA constructor should validate this, but I don't think it does

    return kdmas


def _average_with_default(values: list[int | float] | None) -> float:
    """:return: The average of a list of numbers, or the API-defined mid-value (0.5) if no numbers present."""
    return 0.5 if values is None or len(values) == 0 else mean(values)


def merge_attribute_values(kdma_results: list[list[KDMA]]) -> list[KDMA]:
    """ Combine multiple KDMA lists into a single list.

    :param kdma_results: Each element in the outer list is a result from a single probe.
      Each element in the inner list is a single KDMA value (amongst possibly multiple) from a single probe.
    :return: Combining all like-KDMAs into a single value somehow (average). Although it is a list for the sake of API,
      each name should appear no more than once in the list.
    """
    values_grouped_by_attribute: dict[str, list[int | float]] = defaultdict(list)
    for single_probe_results in kdma_results:
        for kdma in single_probe_results:
            values_grouped_by_attribute[kdma.kdma].append(kdma.value)
    values_grouped_by_attribute = dict(values_grouped_by_attribute)
    # Unless all scores are 0.5, remove all 0.5 scores because they do not affect the attribute
    for k,v in values_grouped_by_attribute.items():
        if not all(i == 0.5 for i in v):
            values_grouped_by_attribute[k] = [x for x in v if x != 0.5]
    mean_kdmas = [KDMA(kdma=name, value=_average_with_default(values)) for name, values in values_grouped_by_attribute.items()]

    return mean_kdmas


def main() -> None:
    """Example code that exercises functionality."""
    all_choices = ["MetricsEval.MD1.1.A", "MetricsEval.MD1.2.A"]
    kdma_values1 = compute_attribute_values(choice_id=all_choices[0], all_choices=all_choices)
    kdma_values2 = compute_attribute_values(choice_id=all_choices[1], all_choices=all_choices)
    combined = merge_attribute_values(kdma_results=[kdma_values1, kdma_values2])
    assert combined is not None
    return


if __name__ == '__main__':
    main()
