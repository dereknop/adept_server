# ADEPT API

This project holds ADEPT's implementation of the In the Moment Metrics Eval TA1 API for characterization and alignment.

[[_TOC_]]

## About

This is a Python Flask server that implements the characterization and alignment interface, based on [Soartech's proposed MVP1 API specification](https://github.com/ITM-Soartech/itm-api/blob/master/itm-api.yaml) ([SHA-1 de45fad78d113](https://github.com/ITM-Soartech/itm-api/commit/de45fad78d113ae85d227e6ed683f834ea18bbf0)), with some modifications to support new concepts for the Metrics Eval.  The application source lives in "openapi_server/", and the rest is supporting files.

### Scenarios

There are two types of [scenarios](openapi_server/data/scenario/metrics_eval).

The first type (MetricsEval.MD3.yaml, MetricsEval.MD17.yaml, & MetricEvalz.MD18.yaml) are scenarios that ADEPT developed for training purposes.  There is also the [training targets](openapi_server/data/alignment_target/metrics_eval) for low (alignment_target_train_LOW.yaml) and high (alignment_target_train_HIGH.yaml).

The second type of scenario is the ADEPT evaluation scenarios (MetricsEval.MD1-Urban.yaml, MetricsEval.MD5-Desert.yaml, MetricsEval.MD6-Submarine.yaml, MetricsEval.MD-Jungle.yaml) and evaluation targets (alignment_target_eval_LOW.yaml & alignment_target_eval_HIGH.yaml).  They have their own targets because due to the scoring in the evaluation scenarios, its impossible to get perfect 0 or 1 values.

Further description of scenarios is available [on the ITM confluence](https://nextcentury.atlassian.net/wiki/spaces/ITMC/pages/3091038209/ADEPT+Metrics+Evaluation+Scenarios).

### Alignment target distribution (optional)

This release contains support for a new type of alignment target called an alignment target distribution.  This is an optional concept not needed for MetricsEval, but for ADEPT it is our next experiment in characterization and alignment.
An alignment target distribution is a list of alignment targets, which represents the KDMA characteristics of a "target population" rather than a single point in KDMA space.  You can view them at openapi_server/data/population_data/metrics_eval.
- Added supporting endpoints (/api/v1/alignment_target_distribution_ids and /api/v1/alignment_target_distribution/{target_id})
- /api/v1/alignment_target/{target_id} now has an optional second argument "population" which if enabled, gets the distribution version of the target.
- Similarly /api/v1/alignment/probe and /api/v1/alignment/session can now take an optional boolean "population".  Enabling this will perform alignment calculation using a Gaussian Mixture Model of the (artificial) population.

## Run locally

Requires Python 3.10 or newer.

From the cloned repository root directory:

### Virtual environment (optional)
```shell
python3 -m venv --clear --upgrade-deps venv
source venv/bin/activate
```
On Windows, the method to activate depends on the shell:
- Git Bash: `source venv/Scripts/activate`
- PowerShell: `venv\Scripts\Activate.ps1`
- cmd.exe: `venv\Scripts\activate.bat`

### Install
```shell
pip install -r requirements.txt
```

### Run
```shell
python -m openapi_server
```
then open browser to http://localhost:8080/ui/  
(You can set a custom port by using --port, e.g. `python -m openapi_server --port 8081`.)

There is also an OpenAPI definition at http://localhost:8080/openapi.json

## Running with Docker

To run the server on a Docker container, please execute the following from the cloned repository root directory:

```shell
# building the image
docker build -t openapi_server .

# starting up a container
docker run -p 8080:8080 openapi_server
```

## Example session

Get scenario:
```shell
curl -X 'GET' \
  'http://localhost:8080/api/v1/scenario/MetricsEval.MD3' \
  -H 'accept: application/json'
```
Start session:
```shell
curl -X 'POST' \
  'http://localhost:8080/api/v1/new_session' \
  -H 'accept: application/json' \
  -d ''
```
> "f0cb2e21-2aa1-4f11-908e-e918d75f57d0"

Post a response:
```shell
curl -X 'POST' \
  'http://localhost:8080/api/v1/response' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "response": {
    "choice": "MetricsEval.MD3.1.A",
    "justification": "justification",
    "probe_id": "MetricsEval.MD3.1",
    "scenario_id": "MetricsEval.MD3"
  },
  "session_id": "f0cb2e21-2aa1-4f11-908e-e918d75f57d0"
}'
```

Get alignment:
```shell
curl -X 'GET' \
  'http://localhost:8080/api/v1/alignment/session?session_id=f0cb2e21-2aa1-4f11-908e-e918d75f57d0&target_id=ADEPT-metrics_eval-alignment-target-train-HIGH&population=false' \
  -H 'accept: application/json'
  ```
>{
>  "alignment_source": [
>    {
>      "probes": [
>        "MetricsEval.MD3.1"
>      ],
>      "scenario_id": "MetricsEval.MD3"
>    }
>  ],
>  "alignment_target_id": "ADEPT-metrics_eval-alignment-target-train-HIGH",
>  "kdma_values": [
>    {
>      "kdma": "MoralDesert",
>      "value": 0
>    }
>  ],
>  "score": 0
>}

## References

- [Client for this server](https://gitlab.com/itm-ta1-adept-shared/adept_client)
- [Info on the scenarios](https://nextcentury.atlassian.net/wiki/spaces/ITMC/pages/3091038209/ADEPT+Metrics+Evaluation+Scenarios)
- [TA3's server](https://github.com/NextCenturyCorporation/itm-evaluation-server)
- [TA3's client](https://github.com/NextCenturyCorporation/itm-evaluation-client)
